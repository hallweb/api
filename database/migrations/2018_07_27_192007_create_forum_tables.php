<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForumTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('forum_users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username');
            $table->string('email')->unique();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('forum_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('description', 255)->nullable();
            $table->unsignedInteger('parent_id')->nullable();
            $table->foreign('parent_id')->references('id')->on('forum_categories')->onDelete('cascade');
        });

        Schema::create('forum_threads', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->text('body');
            $table->string('description', 255);
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('category_id');
            $table->timestamps();

            $table->foreign('category_id')->references('id')->on('forum_categories')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('forum_users');
        });

        Schema::create('forum_replies', function (Blueprint $table) {
            $table->increments('id');
            $table->text('body');
            $table->unsignedInteger('thread_id');
            $table->unsignedInteger('user_id');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('forum_users');
            $table->foreign('thread_id')->references('id')->on('forum_threads')->onDelete('cascade');
        });

        Schema::create('forum_profiles', function(Blueprint $table){
            $table->unsignedInteger('user_id');
            $table->string('name')->default('Bob Esponja');
            $table->string('role_primary')->nullable();
            $table->string('biography', 255)->nullable();
            $table->timestamps();
            $table->primary('user_id');
            $table->foreign('user_id')->references('id')->on('forum_users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('forum_users');
        Schema::dropIfExists('forum_categories');
        Schema::dropIfExists('forum_threads');
        Schema::dropIfExists('forum_replies');
        Schema::dropIfExists('forum_profiles');
        Schema::dropIfExists('forum_users');
        Schema::enableForeignKeyConstraints();
    }
}
