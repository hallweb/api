<?php

use Illuminate\Database\Seeder;
use Hall\Domains\{
    User, 
    Role,
    Forum\Thread,
    Forum\Reply
};


class BouncerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
 
        $this->command->info('Banned');
         $banned = Bouncer::role()->firstOrCreate([
            'name' => 'banned',
            'title' => 'User Banido'
        ]);

        $this->command->info('Civil');
        $civil = Bouncer::role()->firstOrCreate([
            'name' => 'civil',
            'title' => 'Civil'
        ]);

        $this->command->info('SideKick');
        $sidekick = Bouncer::role()->firstOrCreate([
            'name' => 'sidekick',
            'title' => 'Sidekick'
        ]);


        $this->command->info('Hero');
        $hero = Bouncer::role()->firstOrCreate([
            'name' => 'hero',
            'title' => 'Heroi'
        ]);

        $this->command->info('Celestial');
        $celestial = Bouncer::role()->firstOrCreate([
            'name' => 'celestial',
            'title' => 'Celestial'
        ]);

        $this->command->info('Zen-Oh');
        $zen = Bouncer::role()->firstOrCreate([
            'name' => 'admin',
            'title' => 'Zen-Oh'
        ]);

        $this->command->info('Habilidades ...');
        $this->command->info('Login');
        $login = Bouncer::ability()->firstOrCreate([
            'name' => 'login',
            'title' => 'Logar no Sistema'
        ]);

        $this->command->info('Ban Hammer');
        $ban = Bouncer::ability()->firstOrCreate([
            'name' => 'ban-user',
            'title' => 'Ban Hammer'
         ]);            

        $this->command->info('Permissoes');  
        if(!$banned->cannot('*')){
            Bouncer::forbid($banned)->everything();
            $this->command->info('Banned create');
        }
        if($civil->abilities()->where(['title' =>'Manage everything owned'])->count() < 1){
            Bouncer::allow($civil)->toOwnEverything();
            $this->command->info('Civil Can Manage to Own');
        }
       if(!$civil->can('login')){
            Bouncer::allow($civil)->to($login);
            $this->command->info('Civil Can Login');
       }
       if(!$civil->can('create', Thread::class)){
            Bouncer::allow($civil)->to('create', Thread::class);
            $this->command->info('Civil Can Create Thread');
       }
       if(!$civil->can('create', Reply::class)){
            Bouncer::allow($civil)->to('create', Reply::class);
            $this->command->info('Civil Can Create Reply');            
       }
       if(!$hero->can('delete', Thread::class)){
            Bouncer::allow($hero)->toManage(Thread::class);
            $this->command->info('Hero Can Manage Thread');  
       }

       if(!$hero->can('delete', Reply::class)){
            Bouncer::allow($hero)->toManage(Reply::class);
            $this->command->info('Hero Can Manage Reply');  
       }
    //    Bouncer::allow($civil)->to('delete-thread', Thread::class);
    //    Bouncer::allow($civil)->to('delete-reply', Reply::class);
       if(!$zen->can($ban)){
           Bouncer::allow($zen)->to($ban);
            $this->command->info('Zen is the Ban Hammer');
       } 

    }
}
