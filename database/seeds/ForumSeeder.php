<?php

use Illuminate\Database\Seeder;
use Hall\Domains\Forum\Category;

class ForumSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cinema = Category::firstOrCreate([
            'name' => 'Cinema',
            'description' => 'Lugar Para todos os amantes da setima arte',
        ]);

        $hq = Category::firstOrCreate([
            'name' => 'Historias em Quadrinhos',
            'description' => 'Aqui é para todos que busquem o conhecimento sobre HQ'
        ]);

        $games = Category::firstOrCreate([
            'name' => 'Jogos em Geral',
            'description' => 'Venha se aventurar em um mundo sobre jogos de todos os tipos'
        ]);

        $series = Category::firstOrCreate([
            'name' => 'Series de Tv',
            'description' => 'Buscando falar sobre aquele evento que rolou no ultimo episodio.. veio ao lugar certo'
        ]);

        $netflix = Category::firstOrCreate([
            'name' => 'Netflix',
            'description' => 'Não existe lugar melhor que o conforto de casa, vendo uma netflix na tv, não eh mesmo ?!'
        ]);

        $ucMarvel = Category::firstOrCreate([
            'name' => 'Universo Cinematografico da Marvel',
            'description' => 'A Casa das ideias, um lugar bom para se falar de filmes da Marvel',
            'parent_id' => $cinema->id
        ]);

        $UEDC = Category::firstOrCreate([
            'name' => 'Universo Extendido da DC',
            'description' => 'Lugar secreto para ver o que o futuro do UEDC possa nos revelar',
            'parent_id' => $cinema->id
        ]);

        $hqMarvel = Category::firstOrCreate([
            'name' => 'Universo dos Quadrinhos da Marvel',
            'description' => 'A Casa das ideias, um lugar bom para se falar das HQ da Marvel',
            'parent_id' => $hq->id
        ]);

        $hqDC = Category::firstOrCreate([
            'name' => 'Universo DC',
            'description' => 'Lugar secreto para ver o que o futuro da DC nos quadrinhos',
            'parent_id' => $hq->id
        ]);


        if(!Hall\Domains\User::count() <> 0){
            $categories = \Hall\Domains\Forum\Category::where('parent_id', '<>', null)->get();
            $categories->each(function($category){
                    $threads = factory(\Hall\Domains\Forum\Thread::class, rand(4,10))
                        ->create(['category_id' => $category->id]);
                    $threads->each(function ($thread){
                        factory(\Hall\Domains\Forum\Reply::class, rand(5,10))
                            ->create(['thread_id' => $thread->id]);
                    });
            });
        }

    }
}
