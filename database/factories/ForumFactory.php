<?php

use Faker\Generator as Faker;

$factory->define(Hall\Domains\User::class, function (Faker $faker) {
    return [
        'username' => $faker->userName,
        'email' => $faker->unique()->safeEmail,
        'password' => 'secret',
        'remember_token' => str_random(10)
    ];
});

$factory->define(Hall\Domains\Profile::class, function (Faker $faker){
  return [
      'name' => $faker->name,
      'biography' => $faker->sentence,
      'user_id' => function() {
          return factory(Hall\Domains\User::class)->create()->id;
      }
  ];
});

$factory->define(Hall\Domains\Forum\Thread::class, function(Faker $faker){
    return [
        'title' => $faker->sentence,
        'body' => implode(' ', $faker->paragraphs),
        'description' => $faker->text,
        'category_id' => 1,
        'user_id' => function(){
            return factory(Hall\Domains\User::class)->create()->id;
        }
    ];
});

$factory->define(Hall\Domains\Forum\Reply::class, function (Faker $faker) {
    return [
        'body' => $faker->paragraph,
        'user_id' => function(){
            return factory(Hall\Domains\User::class)->create()->id;
        },
        'thread_id' => function(){
            return factory(Hall\Domains\Forum\Thread::class)->create()->id;
        }
    ];
});