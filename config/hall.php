<?php

return [
    /**
     * CONFIGURAÇÕES DO HALL PERSONALIZADAS
     *  -> PASSPORT
     */
    'client_id' => env('PASSPORT_CLIENT_ID'),
    'client_secret' => env('PASSPORT_CLIENT_SECRET'),


    /**
     * env
     */
    'img_url' => env('IMG_URL', 'http://image.localhost/'),
];