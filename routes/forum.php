<?php


Route::get('/', 'CategoriesController@index');

Route::get('/category/{category}', 'CategoriesController@show');

Route::group(['middleware' => ['auth:api']], function(){
    Route::resource('reply', 'RepliesController')->only(['store', 'update', 'destroy']);
    Route::resource('thread', 'ThreadsController')->except(['index', 'create', 'edit']);
    Route::get('user/threads', 'ThreadsController@userThreads');
    Route::get('thread/{thread}/replies', 'ThreadsController@replies')->name('threads.reply');
    Route::get('replies/{id}', 'RepliesController@thread')->name('replies.thread');
});