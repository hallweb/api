<?php

 Route::get('/', 'PostsController@index');
 Route::get('post/{slug}', 'PostsController@show');
 Route::get('featured', 'PostsController@featuredPost');

 Route::get('cat', 'CategoriesController@index');