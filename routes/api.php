<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace' => 'Auth'], function(){
    Route::post('login', 'LoginController@login');
    Route::post('register', 'RegisterController@register');

    Route::group(['middleware' => ['auth:api']], function(){
        Route::get('user', 'LoginController@show');
        Route::get('logout', 'LoginController@logout');
    });
});

Route::group(['middleware' => ['auth:api']], function(){
        Route::get('users', 'UserController@index');
        Route::post('users/profile', 'UserController@updateProfile');
        Route::post('image-upload', 'UserController@photoProfile');

});
