<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::prefix('dash')->group(function () {
    Voyager::routes();
});

Route::domain(Config('hall.img_url'))->group(function (){
    Route::group(['namespace' => 'Media'],  function(){
        Route::group(['prefix' => 'media'], function () {
            // Route::group(['prefix' => 'avatar'], function () {
                Route::get('{id}/{name}', 'ImagesController@avatar');
            // });
            Route::get('default/{name}', 'ImagesController@imageDefault');
        });
        Route::get('storage/{folder}/{name}', 'ImagesController@voyager');
    });

});
