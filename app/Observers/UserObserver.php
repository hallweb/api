<?php

namespace Hall\Observers;

use Hall\Domains\User;
use Bouncer;

class UserObserver
{
     
    public function created(User $user)
    {
        Bouncer::assign('civil')->to($user);
        $user->profile()->create();
    }
}