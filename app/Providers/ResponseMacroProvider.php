<?php

namespace Hall\Providers;

use Illuminate\Support\ServiceProvider;
use Response;

class ResponseMacroProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        Response::macro('hfile', function ($folderOrId, $name, $disk) {
            $storage = \Storage::disk($disk);
            $fileName = $folderOrId . '/' . $name;

            if (!$storage->exists($fileName)) {
                abort(404);
            }
            return response()->file($storage->path($fileName));
        });

        Response::macro('hError', function ($title){
            return response()->json([
                'status' => 'warning',
                'message' => 'algo deu errado ao criar um(a) ' . $err,
                'code' => 422
            ], 422);
        });

        Response::macro('hException', function ($err){
            return response()->json([
                'status' => 'warning',
                'message' => $err->getMessage(),
                'code' => $err->getCode()
            ], 422);
        });

        Response::macro('hSuccess', function($msg, $model){
            return response()->json([
                'status' => 'success',
                'message' => $msg,
                'info' => $model,
                'code' => 200
            ], 200);
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
