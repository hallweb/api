<?php

namespace Hall\Providers;

use Illuminate\Support\ServiceProvider;
use Bouncer;
use Hall\Domains\{User, Role, Ability};

class BouncerProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        Bouncer::tables([
            'roles' => 'forum_roles',
            'abilities' => 'forum_ability',
            'permissions' => 'forum_permissions',
            'assigned_roles' => 'forum_assigned_roles'
        ]);
        Bouncer::useUserModel(User::class);
        Bouncer::useAbilityModel(Ability::class);
        Bouncer::useRoleModel(Role::class);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
