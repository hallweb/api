<?php

namespace Hall\Providers;

use Illuminate\Support\ServiceProvider;
use Carbon\Carbon;
use Illuminate\Support\Facades\Schema;
use Illuminate\Http\Resources\Json\Resource;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        Schema::defaultStringLength(191);
        Resource::withoutWrapping();
        Carbon::setlocale('pt-BR');
        //setLocale(LC_TIME, 'Portuguese');
        setlocale(LC_ALL, "pt_BR", "pt_BR.iso-8859-1", "pt_BR.utf-8", "portuguese");
        date_default_timezone_set('America/Sao_Paulo');

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
       $this->app->register(BouncerProvider::class);
       $this->app->register(ObserverProvider::class);
       $this->app->register(ResponseMacroProvider::class);
    }
}
