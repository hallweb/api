<?php

namespace Hall\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Route;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'Hall\Model' => 'Hall\Policies\ModelPolicy',
        // 'Hall\Domains\Forum\Thread' => 'Hall\Policies\Forum\ThreadPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        // Route::group(['middleware' => ['client.passport']], function(){
        //     \Passport::routes(function ($router){
        //         $router->forAccessTokens();
        //         $router->forTransientTokens();
        //     });
        // });
       
        //
    }
}
