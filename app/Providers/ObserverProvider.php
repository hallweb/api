<?php

namespace Hall\Providers;

use Illuminate\Support\ServiceProvider;
use Hall\Domains\{
    User
};
use Hall\Observers\{
    UserObserver
};

class ObserverProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        User::observe(UserObserver::class);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
