<?php

namespace Hall\Domains\Forum;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'forum_categories';

    protected $fillable = ['name', 'description', 'parent_id'];

    protected $withCount = ['thread'];

    protected $appends = ['is_sub'];

    public $timestamps = false;

    public function thread()
    {
        return $this->hasMany(Thread::class);
    }

    public function subCategory()
    {
        return $this->hasMany(self::class, 'parent_id', 'id');
    }

    public function getIsSubAttribute(){
        return $this->parent_id <> null;
    }

    public function parentId()
    {
        return $this->belongsTo(self::class);
    }

    public function parentIdList()
    {
        return Category::whereNull('parent_id')->get();
    }
}
