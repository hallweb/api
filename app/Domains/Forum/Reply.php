<?php

namespace Hall\Domains\Forum;

use Illuminate\Database\Eloquent\Model;
use Hall\Domains\User;

class Reply extends Model
{
    protected $table = 'forum_replies';

    protected $fillable = ['user_id', 'body', 'thread_id'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function thread()
    {
        return $this->belongsTo(Thread::class);
    }
}
