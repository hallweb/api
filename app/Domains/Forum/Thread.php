<?php

namespace Hall\Domains\Forum;

use Illuminate\Database\Eloquent\Model;
use Hall\Domains\User;
use Laravel\Scout\Searchable;

class Thread extends Model
{
    use Searchable;

    public $asYouType = true;

    protected $table = 'forum_threads';

    protected $casts = ['user_id' => 'integer'];

    protected $withCount = ['replies'];

    protected $dates = ['created_at', 'updated_at'];

    protected $fillable = ['user_id', 'description', 'category_id', 'title', 'body'];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }
    
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function replies()
    {
        return $this->hasMany(Reply::class)->orderBy('updated_at', 'desc');
    }

    public function toSearchableArray()
    {
        $array = $this->toArray();

        return $array;
    }
}
