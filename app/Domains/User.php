<?php

namespace Hall\Domains;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Database\Eloquent\SoftDeletes;
use Silber\Bouncer\Database\HasRolesAndAbilities;
use Bouncer;
use Hall\Domains\Forum\{Thread, Reply};

class User extends Authenticatable
{
    use HasApiTokens, HasRolesAndAbilities, Notifiable, SoftDeletes;

    protected $table = 'forum_users';

    protected $with = ['profile'];

    protected $withCount = ['threads'];

    protected $dates = ['created_at', 'updated_at'];

    protected $fillable = ['username', 'email', 'password'];

    protected $hidden = ['password', 'remember_token'];

    public function role(){
        return $this->roles->first();
    }

    public function profile()
    {
        return $this->hasOne(Profile::class);
    }

    public function threads()
    {
        return $this->hasMany(Thread::class);
    }

    public function replies()
    {
        return $this->hasMany(Reply::class);
    }

    public function setPasswordAttribute($input)
    {
        if ($input)
        $this->attributes['password'] = app('hash')->needsRehash($input) ? \Hash::make($input) : $input;
    }
}
