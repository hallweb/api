<?php

namespace Hall\Domains;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Hall\Domains\User;


class Profile extends Model implements HasMedia
{
    use HasMediaTrait;

    protected $table = 'forum_profiles';

    protected $primaryKey = 'user_id';

    protected $fillable = ['name', 'biography', 'user_id', 'role_primary'];

    public $incrementing = false;
    
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function registerMediaCollections()
    {
        $this->addMediaCollection('avatars')
            ->singleFile();
    }

    public function image()
    {
        $default = \Storage::disk('media')->url('default/avatar.jpeg');
        $avatar = $this->getMedia('avatars')->first();
        return $avatar ? $avatar->getUrl() : $default;
    }

}
