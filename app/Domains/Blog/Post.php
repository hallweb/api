<?php

namespace Hall\Domains\Blog;

use TCG\Voyager\Models\Post as Model;
use Hall\User;

class Post extends Model
{
    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function imageUrl()
    {
        return \Storage::disk('public')->url($this->image);
    }

    public function category()
    {
        return $this->belongsTo(Category::class)->withDefault([
            'name' => 'Category Null',
            'slug' => 'category-null'
        ]);
    }

    public function author()
    {
        return $this->belongsTo(User::class, 'author_id')->withDefault([
            'name' => 'Gaspar'
        ]);
    }
}
