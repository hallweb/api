<?php

namespace Hall\Domains\Blog;

use TCG\Voyager\Models\Category as Model;

class Category extends Model
{
    protected $withCount = ['posts'];


    public function subCategory()
    {
        return $this->hasMany(self::class, 'parent_id', 'id');
    }
}
