<?php

namespace Hall\Domains;

use Silber\Bouncer\Database\Role as Model;

class Role extends Model
{


    public function getRole($name)
    {
        return $self::where(['name' => $name])->firstOrFail();
    }
}
