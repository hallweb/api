<?php

namespace Hall\Domains;
use Silber\Bouncer\Database\Ability as Model;

class Ability extends Model
{
    public function getAbility($name)
    {
        return $self::where(['name' => $name])->firstOrFail();
    }
}
