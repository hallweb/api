<?php

namespace Hall\Policies\Forum;

use Hall\Domains\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Bouncer;
use Hall\Domains\Forum\Thread;

class ThreadPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function create(User $user)
    {
        return Bouncer::can('create', Thread::class);
    }
}
