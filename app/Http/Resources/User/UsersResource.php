<?php

namespace Hall\Http\Resources\User;

use Illuminate\Http\Resources\Json\Resource;
use Hall\Http\Resources\User\Role\RoleResource;

class UsersResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'username' => $this->username,
            'email' => $this->email,
            'name' => $this->profile->name,
            'avatar' => $this->profile->image(),
            'bio' => $this->profile->biography,
            'thread_count' => $this->threads_count,
            'role' => RoleResource::make($this->role())
        ];
    }
}
