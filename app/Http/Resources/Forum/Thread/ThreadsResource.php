<?php

namespace Hall\Http\Resources\Forum\Thread;

use Illuminate\Http\Resources\Json\Resource;
use Hall\Http\Resources\Forum\{
    Category\CategoriesResource as Category,
    Reply\RepliesResource as Replies
};
use Hall\Http\Resources\User\UsersResource as User;
class ThreadsResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'description' => $this->description,
            'body' => $this->body,
            'replies_count' => $this->replies_count,
            'category' => new Category($this->whenLoaded('category')),
            'user' => new User($this->user),
            'replies' => Replies::collection($this->whenLoaded('replies')),
            'created_at' => $this->created_at->formatLocalized('%d de %B de %Y'),
            'updated_at' => $this->when($this->updated_at != $this->created_at, $this->updated_at->formatLocalized('%d de %B de %Y as %T')),            
        ];
    }
}
