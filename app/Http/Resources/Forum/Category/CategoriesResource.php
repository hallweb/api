<?php

namespace Hall\Http\Resources\Forum\Category;

use Illuminate\Http\Resources\Json\Resource;
use Hall\Http\Resources\Forum\Thread\ThreadsResource as Thread;
class CategoriesResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'sub' => CategoriesResource::collection($this->whenLoaded('subCategory')),
            'thread_count' => $this->when($this->is_sub, $this->thread_count),
            'thread' => Thread::collection($this->whenLoaded('thread'))
        ];
    }
}
