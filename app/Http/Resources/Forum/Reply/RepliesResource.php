<?php

namespace Hall\Http\Resources\Forum\Reply;

use Illuminate\Http\Resources\Json\Resource;
use Hall\Http\Resources\User\UsersResource as User;

class RepliesResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'body' => $this->body,
            'user' => User::make($this->user),
            'created_at' => $this->created_at->diffForHumans(),
            'updated_at' => $this->when($this->updated_at != $this->created_at, $this->updated_at->diffForHumans()),
        ];
    }
}
