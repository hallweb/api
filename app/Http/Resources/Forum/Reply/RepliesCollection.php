<?php

namespace Hall\Http\Resources\Forum\Reply;

use Illuminate\Http\Resources\Json\ResourceCollection;

class RepliesCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request);
    }
}
