<?php

namespace Hall\Http\Resources\Blog\Post;

use Illuminate\Http\Resources\Json\Resource;
use Hall\Http\Resources\Blog\{
    Author\AuthorsResource as Author,
    Category\CategoriesResource as Category
};

class PostsResource extends Resource
{

    protected $withOutFields = [];

    public static function collection($resource)
    {
        return tap(new PostsCollection($resource), function ($collection){
            $collection->collects = __CLASS__;
        });
    }
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->filterFields([
            'id' => $this->id,
            'title' => $this->title,
            'description' => $this->excerpt,
            'body' => $this->body,
            'url' => $this->slug,
            'image' => $this->imageUrl(),
            'created_at' => $this->created_at->formatLocalized('%d de %B de %Y'),
            'updated_at' => $this->when($this->updated_at != $this->created_at, $this->updated_at->formatLocalized('%d de %B de %Y')),
            'author' => Author::make($this->author),
            // 'category' => $this->when($this->category_id, Category::make($this->category))
            'category' => Category::make($this->category)
        ]);
    }

    public function hide(array $fields)
    {
        $this->withOutFields = $fields;
        return $this;
    }

    protected function filterFields($array)
    {
        return collect($array)->forget($this->withOutFields)->toArray();
    }
}
