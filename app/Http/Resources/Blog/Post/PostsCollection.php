<?php

namespace Hall\Http\Resources\Blog\Post;

use Illuminate\Http\Resources\Json\ResourceCollection;
use Hall\Domains\Blog\Post;

class PostsCollection extends ResourceCollection
{

    protected $withOutFields = [];
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->processCollection($request);
    }

    public function hide(array $fields)
    {
        $this->withOutFields = $fields;
        return $this;
    }

    protected function processCollection($request)
    {
        return $this->collection->map(function (Post $post) use ($request){
            return PostsResource::make($post)->hide($this->withOutFields)->toArray($request);
        });
    }

}
