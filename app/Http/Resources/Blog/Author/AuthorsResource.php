<?php

namespace Hall\Http\Resources\Blog\Author;

use Illuminate\Http\Resources\Json\Resource;

class AuthorsResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name' => $this->name,
            'avatar' => $this->avatar()
        ];
    }
}
