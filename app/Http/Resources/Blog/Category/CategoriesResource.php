<?php

namespace Hall\Http\Resources\Blog\Category;

use Illuminate\Http\Resources\Json\Resource;
use Hall\Http\Resources\Blog\Post\PostsResource;

class CategoriesResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name' => $this->name,
            'slug' => $this->slug,
            'sub' => CategoriesResource::collection($this->whenLoaded('subCategory')),
            'posts' => PostsResource::collection($this->whenLoaded('posts')),
            'posts_count' => $this->when($this->posts_count, $this->posts_count)
        ];
    }
}
