<?php

namespace Hall\Http\Requests\Forum\Thread;

use Illuminate\Foundation\Http\FormRequest;

class ThreadCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'       => 'required|string|max:255',
            'category_id' => 'required|exists:forum_categories,id',
            'body'        => 'required|min:20|max:65535',
            'description' => 'required|max:255',
        ];
    }

    public function messages()
    {
        return [
            'max' => ':attribute so pode ter no max :max'
        ];
    }
}
