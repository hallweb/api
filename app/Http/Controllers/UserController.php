<?php

namespace Hall\Http\Controllers;

use Illuminate\Http\Request;
use Hall\Domains\User;
use Hall\Http\Resources\User\UsersResource;
use Carbon\Carbon;

class UserController extends Controller
{
    public function index()
    {
        $users = User::all();
        return UsersResource::collection($users);
    }

     public function updateProfile(Request $request){
        $user = $request->user();
        $profile = [
            'biography' => $request->bio,
            'name' => $request->name,
        ];
        // if($request->email != $user->email){
        //      $userUpdate = User::update($request->only('email'), $user->id);
        // }
        $user->update($request->only('email'));
        $user->profile()->update($profile);
        return response()->hSuccess('Profile Updated with Success', UsersResource::make($user));

    }

    public function photoProfile(Request $request)
    {
        try 
        {
            $profile = $request->user()->profile;
            $fileName = md5($profile->user_id . Carbon::now());
            $media = $profile->addMediaFromBase64($request->image)
                ->usingFileName($fileName .'.jpg')
                ->toMediaCollection('avatars');

        } catch (\Exception $e)
        {
            return response()->hException($e);
        }

        return response()->json([
            'message' => 'Avatar Atualizado com Sucesso',
            'code' => 200
        ], 200);
    }

}

