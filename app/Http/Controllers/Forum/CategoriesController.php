<?php

namespace Hall\Http\Controllers\Forum;

use Illuminate\Http\Request;
use Hall\Http\Controllers\Controller;
use Hall\Domains\Forum\Category;
use Hall\Http\Resources\Forum\Category\{
     CategoriesResource
};

class CategoriesController extends Controller
{
    public function index()
    {
        $categories = Category::whereNull('parent_id')->get();
        $categories->load(['subCategory']);
        return CategoriesResource::collection($categories);
    }

    public function show(Category $category)
    {
        $category->load(['thread' => function($query){
            $query->orderBy('id', 'desc');
        }]);

        return CategoriesResource::make($category);
    }
}
