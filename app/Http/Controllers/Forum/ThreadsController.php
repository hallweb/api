<?php

namespace Hall\Http\Controllers\Forum;

use Illuminate\Http\Request;
use Hall\Http\Controllers\Controller;
use Hall\Domains\Forum\Thread;
use Hall\Http\Requests\Forum\Thread\{
    ThreadCreateRequest as CreateRequest, 
    ThreadUpdateRequest as UpdateRequest
};
use Hall\Http\Resources\Forum\Thread\ThreadsResource;
use Hall\Events\Forum\Thread\{newThreadEvent};

class ThreadsController extends Controller
{
   
    public function __construct()
    {
        $this->middleware('user.auth')->only('store');
    }

    public function show(Thread $thread)
    {
        $thread->load(['category']);
        return new ThreadsResource($thread);
    }

    public function userThreads(Request $request)
    {
        $threads = $request->user()->threads;
        return ThreadsResource::collection($threads);
    }

    public function replies(Thread $thread)
    {
        $thread->replies;
        return ThreadsResource::make($thread);
    }

    public function store(CreateRequest $request)
    {
        $this->authorize('create', Thread::class);
        try{
            $thread = Thread::create($request->all());
        } catch (\Exception $e){
            return response()->hException($e);
        }
        // broadcast(new NewThreadEvent($thread));

        return response()->hSuccess('Thread Created With Success', 
                                    ThreadsResource::make($thread));

    }

    public function update(UpdateRequest $request, Thread $thread)
    {
        try{
            $this->authorize('update', $thread);
            $thread->update($request->all());
        } catch (\Exception $e){
            return response()->hException($e);
        }
        return response()->hSuccess('Thread Updated With Success',
                                    ThreadsResource::make($thread)
        );

    }
}
