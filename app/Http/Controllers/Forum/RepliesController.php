<?php

namespace Hall\Http\Controllers\Forum;

use Illuminate\Http\Request;
use Hall\Http\Controllers\Controller;
use Hall\Http\Resources\Forum\Reply\{
    RepliesResource
};
use Hall\Http\Requests\Forum\Reply\{
    ReplyCreateRequest as CreateRequest,
    ReplyUpdateRequest as UpdateRequest
};
use Hall\Domains\Forum\Reply;

class RepliesController extends Controller
{
    public function __construct()
    {
        $this->middleware('user.auth')->only('store');
    }
    public function thread($id)
    {
        $replies = Reply::where('thread_id', $id)
                        ->orderBy('id', 'desc')
                        ->get();
        return RepliesResource::collection($replies);
    }

    public function store(CreateRequest $request)
    {
        $this->authorize('create', Reply::class);
        try
        {
            $reply = Reply::create($request->all());
        } catch(\Exception $e)
        {
             return response()->json([
                'message' => $e->getMessage(),
                'code' => $e->getCode()
            ], $e->getCode());
        }

        return response()->json([
            'message' => 'Reply created with success',
            'reply' => RepliesResource::make($reply),
            'code' => 200
        ], 200);
        

    }
}
