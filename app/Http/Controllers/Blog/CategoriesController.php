<?php

namespace Hall\Http\Controllers\Blog;

use Illuminate\Http\Request;
use Hall\Http\Controllers\Controller;
use Hall\Domains\Blog\Category;
use Hall\Http\Resources\Blog\CategoriesResource;

class CategoriesController extends Controller
{
    public function index()
    {
        $categories = Category::all();
        $categories->load(['subCategory']);
        return CategoriesResource::collection($categories);
    }
}
