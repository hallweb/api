<?php

namespace Hall\Http\Controllers\Blog;

use Illuminate\Http\Request;
use Hall\Http\Controllers\Controller;
use Hall\Domains\Blog\Post;
use Hall\Http\Resources\Blog\Post\{
    PostsCollection, PostsResource
};

class PostsController extends Controller
{
    public function featuredPost()
    {
        $posts = Post::where('status', 'PUBLISHED')
                    ->where('featured', 1)
                    ->orderBy('created_at', 'desc')
                    ->limit(5)
                    ->get();
        return PostsCollection::make($posts)->hide(['body']);
    }

    public function index()
    {
        $posts = Post::whereStatus('PUBLISHED')
                    ->whereFeatured(0)
                    ->orderBy('created_at', 'desc')
                    ->paginate(10)->withPath('');
        return PostsCollection::make($posts)->hide(['body']);
    }

    public function show($slug)
    {
        $post = Post::whereSlug($slug)
                    ->firstOrFail();
        return PostsResource::make($post);
    }
}
