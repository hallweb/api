<?php

namespace Hall\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Hall\Http\Controllers\Controller;
use Hall\Domains\{User, Profile};
use Illuminate\Support\Facades\{DB, Hash};
use Hall\Http\Resources\User\UsersResource;

class RegisterController extends Controller
{
    public function register(Request $request)
    {
        DB::beginTransaction();
        try{
            $user = User::create($request->all());
            $profile = $user->profile()->update(['user_id' => $user->id, 'name' => $request->name]);
            // $profile = $user->profile()->create(['name' => $request->name]);
            // $profile = Profile::create([
            //     'name' => $request->name,
            //     'user_id' => $user->id
            // ]);
        } catch(\Throwable $e)
        {
            DB::rollback();
            throw $e;
        } 
        DB::commit();
        return response()->json([
            'message' => 'Usuario criado com sucesso',
            'user' => $user,
            'code' => 200
        ], 200);
    }
}
