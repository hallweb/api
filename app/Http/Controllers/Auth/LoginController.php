<?php

namespace Hall\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Hall\Http\Controllers\Controller;
use Hall\Domains\User;
use Illuminate\Support\Facades\{DB, Hash};
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Hall\Http\Resources\User\UsersResource;
use Auth;

use Carbon\Carbon;
class LoginController extends Controller
{

    public function login(Request $request)
    {
        $user = User::where('email', $request->username)->first();

        if(!$user){
            return response()->json([
                'status' => 'warning',
                'error' => 'Usuario '. $request->username .' não encontrado',
                'code' => '422',
            ], 422);
        }

        if(!$user->can('login')){
            return response()->json([
                'status' => 'error',
                'error' => 'Usuario ' . $user->username .' esta banido!!!',
                'code' => '422',
            ], 422);
        }

        // if(!Hash::check($request->password, $user->password)){
        //     return response()->json([
        //         'status' => 'warning',
        //         'error' => 'Senha invalida',
        //         'code' => '422'
        //     ], 422);
        // }
        if(!Auth::guard('forum')->attempt(['email' => $request->username, 'password' => $request->password])) {
            return response()->json([
                'message' => 'Unauthorized',
            ], 401);
        }

        $currentUser = Auth::guard('forum')->user();
        $tokenResult = $currentUser->createToken('Hall');
        $token = $tokenResult->token;
        
        if ($request->remember_me) {
            $token->expires_at = Carbon::now()->addWeeks(1);
            $token->save();
        }
        

        return response()->json([
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_in' => Carbon::parse($tokenResult->token->expires_at)->toDateTimeString()
        ]);

    }

    public function show(Request $request)
    {
        $user = [
            'data' => UsersResource::make($request->user())
        ];
        return response()->json($user);
    }

    public function logout(Request $request)
    {
        $accessToken = $request->user()->token()->revoke();

        return response()->json([
           'status'  => 'success',
           'message' => 'Deslogado com sucesso',
           'code'    => 200
       ], 200);
    }
}
