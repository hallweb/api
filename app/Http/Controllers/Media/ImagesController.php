<?php

namespace Hall\Http\Controllers\Media;

use Illuminate\Http\Request;
use Hall\Http\Controllers\Controller;
use Hall\Domains\Media;

class ImagesController extends Controller
{
    public function avatar($id, $name)
    {
        $file = Media::find($id);

        if($file){
            return response()->file($file->getPath());
        }
        
        return response()->hfile($id, $name, 'media');
    }

    public function voyager($folder, $name)
    {
        return response()->hfile($folder, $name, 'public');
    }

    public function imageDefault($name)
    {
        // $storage = \Storage::disk('media');
        // $fileName = 'defaults/'. $name;
        
        // if(!$storage->exists($fileName)){
        //     abort(404);
        // }
        // return response()->file($storage->path($fileName));
        return response()->hfile('defaults', $name, 'media');
    }
}
