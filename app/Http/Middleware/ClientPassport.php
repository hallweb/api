<?php

namespace Hall\Http\Middleware;

use Closure;

class ClientPassport
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $client = [
            'client_id' => Config('hall.client_id'),
            'client_secret' => Config('hall.client_secret'),
            'grant_type' => 'password'
        ];
        $request->merge($client);
        return $next($request);
    }
}
