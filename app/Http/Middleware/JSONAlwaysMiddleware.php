<?php

namespace Hall\Http\Middleware;
use Illuminate\Http\Request;
use Closure;

class JSONAlwaysMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $request->headers->set("Accept", "application/json");
        return $next($request);
    }
}
